Live webapp: https://johnlawrenceco.gitlab.io/capstone2-co/

Tech Used:
HTML, CSS, JS - CLIENT
MongoDB, ExpressJS, NodeJS - API

API Repo: git@gitlab.com:zuitt-coding-bootcamp-curricula/courses/wdc028/wdc028-35.git

Admin Credentials
email: admin@mail.com
password: hello123

Sample user with already enrolled courses:
email: michaelscott@mail.com
password: 12341234
(Check the Profile section for the list of his enrolled courses)

Features:
User
    - Register
    - Login
    - Enroll a course
    - Profile

Courses
    - Create a Course (Admin only)
    - Retrieve all Courses
    - Update a Course (Admin only)
    - Delete a Course (Admin only)

Folder Structure:
    - Root Folder
        - index.html (landing page)
        - template.html (basic bootstrap html skeleton)
        - assets folder
        - pages folder
            - pages of the webapp
        - assets Folder
            - css
            - images
            - js
                - each page has its own external js file
                - script.js file is linked to ALL html files for a dynamic navbar layout






