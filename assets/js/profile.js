let adminUser = localStorage.getItem('isAdmin');
let token = localStorage.getItem('token');


fetch("https://morning-cliffs-47850.herokuapp.com/api/users/details", {
	headers: {
		"Authorization": `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	console.log(data);
	let profile = document.querySelector("#profile");

	if (adminUser === "false") {
		profile.innerHTML = `
			<h2 id="name" class="mt-5 header-profile">${data.firstName} ${data.lastName}</h2>
			<p id="mobileNo" class="lead">mobile number: ${data.mobileNo}</p>
		`;

		let enrolledCoursesArray = [];

		data.enrollments.map(course => {
			console.log(course)
			//course.courseId
			//course.enrolledOn
			let courses = document.querySelector("#coursesSection");
			

			fetch(`https://morning-cliffs-47850.herokuapp.com/api/courses/${course.courseId}`)
			.then(res => res.json())
			.then(fetchedCourse => {
				enrolledCoursesArray.push(`
					<div class="col-sm-6 col-md-4 col-lg-3 my-3">
						<div class="card card-profile">
							<div class="card-header">
								<h5 class="card-title">${fetchedCourse.name}</h5>
							</div>
							<div class="card-body">
								<p class="card-text" id="courseDesc">Description: ${fetchedCourse.description}</p>
								<p class="card-text">Enrolled on: ${course.enrolledOn}</p>
								<p class="card-text">Status: ${course.status}</p>
							</div>
						</div>
					</div>
				`);

				let joinedCoursesArray = enrolledCoursesArray.join("");

				/* if (enrolledCoursesArray < 1) */

				courses.innerHTML = joinedCoursesArray;
			});		
		});
	} else {
		profile.innerHTML = 
		`
			<h2 id="name" class="mt-5 header-profile">No Profile Available</h2>
		`;
	}
});