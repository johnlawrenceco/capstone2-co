let registerForm = document.querySelector("#registerForm");

// .addEventListener("event_name", callbackFunction())						
//e stands for event. It's standard practice.
registerForm.addEventListener("submit", (e) => {
	e.preventDefault();
	//prevents from deleting input if form is not finished or there's error in the input
	//prevents default disallows the refresh of your page when submitting.

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;
	console.log(firstName);
	console.log(lastName);
	console.log(mobileNumber);
	console.log(email);
	console.log(password1);
	console.log(password2);

	if ((password1 !== '' && password2 !== '') && 
		(password1 === password2) &&
		(mobileNumber.length === 11)) {

		/*
		fetch is a built in js function that allows us to get data from another source without the need to refresh the page.
		This allows us to get a response if an email we are trying to register has already been registered in our database
		
		fetch sends the data to the url provided with its parameters:

		fetch(<url>, <parameters (in object, see below) >)

		parameters may contain:
		//method -> http method (should reflect the http method as defined in your backend)
		//headers 
			-> Content-Type - defines what kind of data to send.
			-> authorization -> contains our Bearer Token
		//body -> the body of our request or req.body

		*/

		// fetch() creates a request. It will try to access route from the backend
		fetch('https://morning-cliffs-47850.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json' //means the type of information passed is JSON
			},
			body: JSON.stringify({
				email: email //converts into string: email="<input email>"

				//req in backend looks like:
				/*
					req {
						headers {
							content-type: "application/json"
						},
						body: {
							email: "<email>"
						},
						etc...
					}
				*/
			})
		})
		.then(res => res.json()) // the return from the route function
		//data is the response now in a json format.
		.then(data => {

			/*
			data => true or false
			if true, then the email already exists in our database.
			if false, then the email has yet to be registered.
			This will check if the email exists or not. 
			If the email exists, then it will register the user. If not, we're going to show an alert.
			*/
			if (data === false) {
				//nest a fetch request using the registration to register our user if the email being registered does not already exists in our database.
				fetch('https://morning-cliffs-47850.herokuapp.com/api/users/', {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName:firstName,
						lastName:lastName,
						email:email,
						password:password1,
						mobileNo: mobileNumber
					})
				})
				.then(res => res.json())
				.then(data => {
					//in our registration, true is sent as a response for successful registration, false if registration has failed
					if (data === true) {
						alert('Registration successful');

						//this method allows us to replace the current document with the document provided in the method which means that for a successful registration, we will be redirected to our login page.
						window.location.replace('./login.html');

					} else {
						alert('Registration failed.');
					}
				});
			} else {
				alert("Email already exists.");
			}
		});
	} else {
		alert('Invalid input');
	}
});