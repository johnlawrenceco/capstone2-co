let navItems = document.querySelector("#navSession");

// localStrage - an object used to store information locally indefinitely in our devices
let userToken = localStorage.getItem("token");

/*
	localStorage = {
		token: "2lk2hn5k2jh52lj5j2kl345j"
		isAdmin: false,
		getItem: function(),
		setItem: function()
		etc...
	}
*/

//conditional rendering:
//dynamically add or delete an html elementbased on a condition
//Here, we are adding our login and register links in the navbar ONLY if the user is not logged in.
//Otherwise, only the log out link will be shown.
if (!userToken) {
	navItems.innerHTML +=
	`
		<li class="nav-item ">
			<a href="./login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item ">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
	`;
} else {
	navItems.innerHTML +=
	`
		<li class="nav-item ">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
		
		<li class="nav-item ">
			<a href="./logout.html" class="nav-link"> Logout </a>
		</li>
	`
}