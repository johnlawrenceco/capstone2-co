let navItems = document.querySelector("#navSession");

let userToken = localStorage.getItem("token");

if (!userToken) {
	console.log(navItems.innerHTML); //this is before we access and add 2 more li to the innerHTML
	navItems.innerHTML +=
	`
		<li class="nav-item ">
			<a href="./pages/login.html" class="nav-link"> Log in </a>
		</li>

		<li class="nav-item ">
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>
	`;
	console.log(navItems.innerHTML);
} else {
	navItems.innerHTML +=
	`
		<li class="nav-item ">
			<a href="./pages/profile.html" class="nav-link"> Profile </a>
		</li>

		<li class="nav-item ">
			<a href="./pages/logout.html" class="nav-link"> Logout </a>
		</li>
	`
}

/*Mini Activity*/

//Conditionally render a greeting to our user when he is logged in.
//If the user is logged in, show a text in the newly created h3 that says Hello, User!
//If not, show a text that says Hello, Guest!

//Stretch Goal:
//If the user is logged in, check if he is an admin. If the logged in user is an admin, show the text, Hello, Admin!
//If the user is a regular user, show only, Hello, User!
let userGreeting = document.querySelector("#userGreeting");
let isAdmin = localStorage.getItem("isAdmin");
console.log(isAdmin); //Data type? Getting items from the localStorage, they are strings.
if (!userToken) {
	userGreeting.innerHTML += "Hello, Guest!";
} else {

	if (isAdmin === "true") { //Data type? Getting items from the localStorage, they are strings.
		userGreeting.innerHTML += "Hello, Admin!";
	} else {
		userGreeting.innerHTML += "Hello, User!";
	}
}