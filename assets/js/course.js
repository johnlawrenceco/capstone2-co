let params = new URLSearchParams(window.location.search); 
let courseId = params.get('courseId');
let adminUser = localStorage.getItem('isAdmin');
let token = localStorage.getItem('token');

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
let enrollees = document.querySelector("#enrollees");

//get our course details
fetch(`https://morning-cliffs-47850.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	console.log(data);
	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;
	
	if (adminUser === "true") {
		let enrolleesNameArray = [];

		data.enrollees.map(enrollee => {
			console.log(enrollee);
			fetch(`https://morning-cliffs-47850.herokuapp.com/api/users/${enrollee.userId}`, {
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(fetchedUser => {
				enrolleesNameArray.push(`
					<div class="col-md-6 my-2">
						<div class="card card-course">
							<div class="card-header">
								<h5 class="card-title">${fetchedUser.lastName}, ${fetchedUser.firstName}</h5>
							</div>
							<div class="card-body">
								<p class="card-text">Enrolled on: ${enrollee.enrolledOn}</p>
							</div>
						</div>
					</div>
				`);
				let joinedEnrolleesArray = enrolleesNameArray.join("");
				enrollees.innerHTML = joinedEnrolleesArray;
			});
		});

	} else {
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary" >Enroll</button>`;

		document.querySelector("#enrollButton").addEventListener("click", () => {
			if (!token) {
				window.location.replace("./login.html")
			} else {
				fetch('https://morning-cliffs-47850.herokuapp.com/api/users/enroll', {
					method: 'POST',
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${token}`
					},
					body: JSON.stringify({
						courseId: courseId
					})
				})
					.then(res => res.json())
					.then(data => {
						if (data) {
							alert("You have enrolled successfully.");
							window.location.replace("./courses.html");
						} else {
							alert("Enrollment Failed.")
						}
					});
			}
		});
	}
});