/*Get the login form and store in a variable*/
let loginForm = document.querySelector("#logInUser");

/*Add a submit event to our login form so that when the button is clicked to submit, we are able to run a function.*/
loginForm.addEventListener("submit", e => {
	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	/*console.log(email);
	console.log(password);*/

	//Check if our user was able to submit an email or password. If not, we'll show an alert. If yes, we'll proceed to the login fetch request.

	if (email == "" || password == "") {
		alert("Please input your email/password.");
	} else {
		/*
			Add a fetch request which will log in our user.

			-In the fetch request input, the correct URL to use the proper route to our login.
			-Enter the correct parameter.
				method,
				headers,
				body
			-Our request body for login only needs 2 input for email and password.
			-Convert the server's response to JSON.
			-After converting the JSON, use console.log() to check the data returned.

			This is similar to your registration.

			Take a screenshot of your code as activity-1.
		*/

		fetch('https://morning-cliffs-47850.herokuapp.com/api/users/login', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			//Store our token in our localStorage.
			//localStorage is a storage for data in most modern browsers.
			//localStorage, however, can only store strings.

			//Check if data/response is false or null. If there is no token or false is returned, we will not save it in the localStorage
			if (data !== false && data.accessToken !== null) {
				//If we are able to get a token, we'll then use it to get our user's details.

				//Store the token in the localStorage:
				//setItem(<nameOfTheKey>, <value>)
				localStorage.setItem('token', data.accessToken);

				fetch('https://morning-cliffs-47850.herokuapp.com/api/users/details', {
					//method is by default, 'GET', so no need to type method: 'GET'

					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					localStorage.setItem('id', data._id);
					localStorage.setItem('isAdmin', data.isAdmin);
					localStorage.setItem('courses', JSON.stringify(data.enrollments));
					//console.log(localStorage);

					//Redirect to our courses.html page.
					window.location.replace('./courses.html');
				});
			} else {
				alert('Login failed. Something went wrong.');
			}
		});
	}
});