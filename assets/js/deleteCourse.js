//We use URLSearchParams to access the specific parts of a query string in the URL
// /course-booking-client/pages/deleteCourse.html?courseId=5fc74808f64de42ca856987d
let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");

//How do we get the token from the localStorage?
let token = localStorage.getItem("token");

//this fetch will be run automatically once you get to the page deleteCourse page.
fetch(`https://morning-cliffs-47850.herokuapp.com/api/courses/deactivate/${courseId}`, {
	method: "PUT",
	headers: {
		//no Content-Type since no need to to input anything in body.
		"Authorization": `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	if (data) {
		alert("Course archived.");
		window.location.replace("./courses.html");
	} else {
		alert("Something went wrong.");
	}
})