let adminUser = localStorage.getItem('isAdmin');
let token = localStorage.getItem('token');
let addButton = document.querySelector('#adminButton');
let subHeaderCourses = document.querySelector('#main-courses > h2');
let courseData;

if (adminUser === "true") {
	subHeaderCourses.innerHTML = "Courses"

	addButton.innerHTML +=
		`	<div class="col-md-2">
				<a href="./addCourse.html" class="btn btn-primary" role="button" id="add-course">Add Course</a>
			</div>
		`;

	fetch('https://morning-cliffs-47850.herokuapp.com/api/courses/all', {
		headers: {
			"Authorization": `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);
		if (data.length < 1) {
			courseData = "No Course Available";
		} else {

			courseData = data.map(course => {
				let goToCourseButton = 
				`
					<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block" id="go-to-course">Go to Course</a>
				`;

				let actDeactButton;

				if (course.isActive) {
					actDeactButton =
					`
						<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block act-deact">Archive Course</a>
					`;
				} else {
					actDeactButton =
					`
						<a href="./reactivateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block act-deact">Reactivate Course</a>
					`;
				}

				return (`
					<div class="col-sm-6 col-md-4 col-lg-3 my-3">
						<div class="card card-courses">
							<div class="card-header">
								<h5 class="card-title">${course.name}</h5>
							</div>
							<div class="card-body">
								<p class="card-text" id="courseDesc">Description: ${course.description}</p>
								<p class="card-text">₱ ${course.price}</p>
							</div>
							<div class="card-footer">
								${goToCourseButton}
								${actDeactButton}
							</div>
						</div>
					</div>
				`)
			}).join("");
		}

		let container = document.querySelector("#coursesContainer");

		container.innerHTML = courseData;
	});
} else {
	addButton.innerHTML = null;

	fetch('https://morning-cliffs-47850.herokuapp.com/api/courses/')
	.then(res => res.json())
	.then(data => {
		console.log(data);
		if (data.length < 1) {
			courseData = "No Course Available";
		} else {

			courseData = data.map(course => {
				let goToCourseButton = 
				`
					<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block" id="go-to-course">Go to Course</a>
				`;

				return (`
					<div class="col-sm-6 col-md-4 col-lg-3 my-3">
						<div class="card card-courses">
							<div class="card-header">
								<h5 class="card-title">${course.name}</h5>
							</div>
							<div class="card-body">
								<p class="card-text" id="courseDesc">Description: ${course.description}</p>
								<p class="card-text">₱ ${course.price}</p>
							</div>
							<div class="card-footer">
								${goToCourseButton}
							</div>
						</div>
					</div>
				`)
			}).join("");
		}

		let container = document.querySelector("#coursesContainer");

		container.innerHTML = courseData;
	});
}